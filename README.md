**Überblick**  
Diese Arbeit beschäftigt sich mit der Frage, wie man den 
Klimawandel und 
weitere klimarelevante Themen in den BM-Physikunterricht einbetten kann. Sie 
bietet einen Überblick über die Thematik und konkrete Umsetzungen. Es ist 
keine vollständige Thematisierung, sondern ein Ansatz für Lehrpersonen, welche 
sich ebenso mit dieser Frage beschäftigen. Die Ideen zur Umsetzung sind 
hauptsächlich qualitativ und möchten das Verständnis stärken. Insbesondere 
sollen dadurch die Themen Wärmestrahlung, Strahlungsgleichgewicht, spezifische 
Wärmekapazität, Schmelzwärme, Wärmeausdehnung, Rückkopplung und Kipp- Effekte, 
Energie, Leistung, Wirkungsgrad und Trägheit in Bezug auf das Klima 
thematisiert werden. Die Lernziele werden durch stufengerechte Aufgaben 
gestützt.

**Errata**  
Fehler und Verbesserungsvorschläge können über Google Docs 
rückgemeldet 
werden: 
https://docs.google.com/document/d/1lE1MKisGqZf_59tZg8JEm5wfTwe1A4HqgcukrUCTAv4/edit?usp=sharing

Rückmeldungen aller Art werden auch per E-Mail entgegengenommen.

**Mitwirken**  
Jeder darf an dem Projekt mitwirken und es verbessern. 
Dazu eignet sich die Zusammenarbeit über Git. Änderungen können in  einem 
neuen Branch (feature/branch-name) gemacht werden. Für Unterstützung im 
Umgang mit Git stehe ich gerne zur Verfügung.

**Kontakt**  
Luca Zurmühle
lucazurm@gmail.com
